import static org.junit.Assert.*;

import org.junit.Test;

public class JogoDaVidaTest {

	@Test
	public void test() {
		fail("Not yet implemented");
	}
	
	@Test
	public void testJogo1() {
		
		
		boolean[][] vMat = new boolean[5][5];
		boolean[][] vEsperado = new boolean[5][5];
		for (int i = 0; i < vMat.length; i++) {
			for (int j = 0; j < vMat.length; j++) {
				vMat[i][j] = false;
				vEsperado[i][j] = false;
			}
		}
		
		vMat[1][2] = true;
		vMat[1][3] = true;
		vMat[1][4] = true; 
		 
		vEsperado[0][3] = true;
		vEsperado[1][3] = true;
		vEsperado[2][3] = true;
		
		//preencher vEsperado com os quadros certos
		assertArrayEquals(vEsperado, JogoDaVida.jogoVida(vMat));

		assertEquals(false, JogoDaVida.jogoVida(vMat));
	}
}
