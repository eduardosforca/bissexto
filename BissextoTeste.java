package bissexto;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class BissextoTeste {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void test2015() {
		assertEquals(false, Bissexto.verificaBissexto(2015));
	}
	
	@Test
	public void test1972() {
		assertEquals(true, Bissexto.verificaBissexto(1972));
	}
	
	
	@Test
	public void test200() {
		assertEquals(false, Bissexto.verificaBissexto(200));
	}
	
	@Test
	public void test400() {
		assertEquals(true, Bissexto.verificaBissexto(400));
	}

}
